"""
URL configuration for expenses project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect

# This feature is about redirecting http://localhost:8000/
# to the receipt list page created in Feature 5.
# In the expenses urls.py, write a function that redirects to
# the name of the path for the list view that you created in the
# previous feature.


def redirect_to_home(request):
    return redirect("home")


urlpatterns = [
    path("", redirect_to_home),
    path("admin/", admin.site.urls),
    path("receipts/", include("receipts.urls")),
    path("accounts/", include("accounts.urls")),
]
