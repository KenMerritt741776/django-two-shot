from django.contrib import admin
from receipts.models import ExpenseCategory, Receipt, Account

# Register your models here.


@admin.register(ExpenseCategory)
class ExpenseCategory(admin.ModelAdmin):
    list_display = [
        "name",
    ]


@admin.register(Receipt)
class Receipt(admin.ModelAdmin):
    list_display = [
        "vendor",
        "total",
        "tax",
        "date",
    ]


@admin.register(Account)
class Account(admin.ModelAdmin):
    list_display = [
        "name",
        "number",
    ]
